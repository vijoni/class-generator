<?php

declare(strict_types=1);

namespace %namespace%;

use Vijoni\ClassGenerator\ClassBase;

class %className% extends ClassBase
{
%classBody%
}
