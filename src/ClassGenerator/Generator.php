<?php

declare(strict_types=1);

namespace Vijoni\ClassGenerator;

use ArrayIterator;
use Iterator;

class Generator
{
  private string $srcDirPath = '';
  private string $indentation = '';

  private const DEFAULT_TEMPLATE_FILE_PATH = __DIR__ . '/class-schema.tpl';

  public function __construct(string $srcDirPath, private SchemaFinder $schemaFinder, int $indentSize = 2)
  {
    $this->srcDirPath = rtrim($srcDirPath, '/');
    $this->indentation = str_repeat(" ", $indentSize);
  }

  /**
   * @param string $schemaFileGlobPattern
   * @param string $templateFilePath
   * @return Schema[]
   */
  public function generate(string $schemaFileGlobPattern, string $templateFilePath = ''): array
  {
    $templateFilePath = $templateFilePath ?: self::DEFAULT_TEMPLATE_FILE_PATH;
    $template = (string)file_get_contents($templateFilePath);

    $schemas = $this->schemaFinder->findSchemas($this->srcDirPath, $schemaFileGlobPattern);
    $deferredSchemas = [];

    do {
      $totalSkipped = count($deferredSchemas);
      $deferredSchemas = $this->generateClassFiles($schemas, $template);
      $schemas = $deferredSchemas;
    } while ($totalSkipped != count($deferredSchemas));

    /** @var Schema[] */
    return $deferredSchemas->getArrayCopy();
  }

  /**
   * @param Iterator|Schema[] $schemas
   * @param string $template
   * @return ArrayIterator
   */
  private function generateClassFiles(Iterator $schemas, string $template): ArrayIterator
  {
    $deferredSchemas = [];
    foreach ($schemas as $schema) {
      if ($this->dependsOnMissingClasses($schema)) {
        $deferredSchemas[] = $schema;
        continue;
      }
      $classContents = $this->buildClassContents($schema, $template);
      $classFilePath = $this->buildClassFilePath($schema);
      @mkdir(dirname($classFilePath), 0660, true);
      file_put_contents($classFilePath, $classContents);
      // required to force load dependent class after autoloader failed to load not yet generated class
      // class_exists() triggers autoloader
      require_once($classFilePath);
    }

    return new ArrayIterator($deferredSchemas);
  }

  private function dependsOnMissingClasses(Schema $schema): bool
  {
    foreach ($schema->fields as $field) {
      if (!$this->isPrimitiveType($field->rawType) && !class_exists($field->rawType)) {
        return true;
      }
    }

    return false;
  }

  private function buildClassFilePath(Schema $schema): string
  {
    return $this->srcDirPath . '/' . ltrim($schema->outputPath, '/') . '/' . $schema->className . '.php';
  }

  private function buildClassContents(Schema $schema, string $template): string
  {
    $fields = $schema->fields;

    $classBody = $this->createClassConstants($schema->constants);
    $classBody .= $this->createClassFields($fields);
    $classBody .= $this->createFieldsKeys($fields);
    $classBody .= $this->createConstructor($fields);
    $classBody .= $this->createAccessorMethods($fields);
    $classBody .= $this->createToArrayMethods($fields);
    $classBody .= $this->createFromArrayMethods($fields);
    $classBody .= $this->createGetFieldKeysMethod($fields);

    $classBody = $this->indentation . trim($classBody);

    return $this->applyClassBasics($schema, $classBody, $template);
  }

  private function applyClassBasics(Schema $schema, string $classBody, string $template): string
  {
    return str_replace(
      [
        '%namespace%',
        '%className%',
        '%classBody%',
      ],
      [
        $schema->namespace,
        $schema->className,
        $classBody,
      ],
      $template
    );
  }

  /**
   * @param Constant[] $constants
   * @return string
   */
  private function createClassConstants(array $constants): string
  {
    $constantsContent = "\n";
    foreach ($constants as $constant) {
      $code = sprintf('public const %s = %s;', $constant->name, $constant->value);
      $constantsContent .= "\n{$this->indentation}" . $code;
    }

    return $constantsContent;
  }

  /**
   * @param Field[] $fields
   * @return string
   */
  private function createClassFields(array $fields): string
  {
    $privatePropertiesContent = "\n";
    foreach ($fields as $field) {
      $privatePropertiesContent .= $this->buildFieldPropertyDefinition($field);
    }

    return $privatePropertiesContent;
  }

  /**
   * @param Field[] $fields
   * @return string
   */
  private function createFieldsKeys(array $fields): string
  {
    $fieldsKeysContent = "\n";
    foreach ($fields as $field) {
      $code = sprintf("public const %s = '%s';", $field->keyName, $field->name);
      $fieldsKeysContent .= "\n{$this->indentation}" . $code;
    }

    return $fieldsKeysContent;
  }

  /**
   * @param Field[] $fields
   * @return string
   */
  private function createConstructor(array $fields): string
  {
    $indent = $this->indentation;
    $constructorBody = '';
    $newLine = '';
    foreach ($fields as $field) {
      if ($this->isChildOfBaseClass($field->type)) {
        $setMethodName = $this->createSetterMethodName($field->name);
        $constructorBody .= "{$newLine}{$indent}{$indent}\$this->{$setMethodName}(new {$field->type}());";
        $newLine = "\n";
      }
    }

    if ($constructorBody !== '') {
      return <<<PHP


{$indent}public function __construct()
{$indent}{
{$constructorBody}
{$indent}}
PHP;
    }

    return '';
  }

  /**
   * @param Field[] $fields
   * @return string
   */
  private function createAccessorMethods(array $fields): string
  {
    $accessorMethodsContent = '';
    foreach ($fields as $field) {
      $accessorMethodsContent .= "\n\n" . $this->buildGetterMethod($field);
      $accessorMethodsContent .= "\n\n" . $this->buildSetterMethod($field);
    }

    return $accessorMethodsContent;
  }

  /**
   * @param Field[] $fields
   * @return string
   */
  private function createToArrayMethods(array $fields): string
  {
    $toArrayMethodDefinition = $this->buildToArrayMethod($fields);
    $toDeepArrayMethodDefinition = $this->buildToDeepArrayMethod($fields);

    return <<<PHP


{$this->indentation}{$toArrayMethodDefinition}

{$this->indentation}{$toDeepArrayMethodDefinition}
PHP;
  }

  /**
   * @param Field[] $fields
   * @return string
   */
  private function createFromArrayMethods(array $fields): string
  {
    $fromArrayMethodDefinition = $this->buildFromArrayMethod($fields);

    return <<<PHP


{$this->indentation}{$fromArrayMethodDefinition}
PHP;
  }

  /**
   * @param Field[] $fields
   * @return string
   */
  private function createGetFieldKeysMethod(array $fields): string
  {
    $indent = $this->indentation;
    $tripleIndent = str_repeat($indent, 3);
    $fieldKeys = '';
    foreach ($fields as $field) {
      $fieldKeys .= $tripleIndent . sprintf("self::%s => true,\n", $field->keyName);
    }
    $fieldKeys = rtrim($fieldKeys);

    return <<<PHP


{$indent}public function fieldKeys(): array
{$indent}{
{$indent}{$indent}return [
{$fieldKeys}
{$indent}{$indent}];
{$indent}}
PHP;
  }

  private function buildFieldPropertyDefinition(Field $field): string
  {
    if ($field->defaultValue) {
      $code = sprintf('private %s $%s = %s;', $field->type, $field->name, $field->defaultValue);
      return "\n{$this->indentation}" . $code;
    }

    $code = sprintf('private %s $%s;', $field->type, $field->name);

    return "\n{$this->indentation}" . $code;
  }

  private function buildGetterMethod(Field $field): string
  {
    $getMethodName = $this->createGetterMethodName($field->name);
    $indent = $this->indentation;

    $docblock = $this->buildGetterMethodDocblock($field);
    $methodSourceCode = <<<PHP
{$indent}public function {$getMethodName}(): {$field->type}
{$indent}{
{$indent}{$indent}return \$this->{$field->name};
{$indent}}
PHP;

    return $docblock . $methodSourceCode;
  }

  private function buildSetterMethod(Field $field): string
  {
    $setMethodName = $this->createSetterMethodName($field->name);
    $indent = $this->indentation;

    return <<<PHP
{$indent}public function {$setMethodName}({$field->type} \${$field->name}): void
{$indent}{
{$indent}{$indent}\$this->{$field->name} = \${$field->name};
{$indent}{$indent}\$this->modified[self::{$field->keyName}] = true;
{$indent}}
PHP;
  }

  private function createGetterMethodName(string $fieldName): string
  {
    return 'get' . ucfirst($fieldName);
  }

  private function createSetterMethodName(string $fieldName): string
  {
    return 'set' . ucfirst($fieldName);
  }

  /**
   * @param Field[] $fields
   * @return string
   */
  private function buildToArrayMethod(array $fields): string
  {
    $methodBody =  '';
    $indent = $this->indentation;
    $tripledIndentation = str_repeat($indent, 3);

    foreach ($fields as $field) {
      $getMethodName = $this->createGetterMethodName($field->name);
      $code = sprintf('self::%s => $this->%s(),', $field->keyName, $getMethodName);
      $methodBody .= "{$tripledIndentation}" . $code . "\n";
    }
    $methodBody = rtrim($methodBody);

    return <<<PHP
public function toArray(): array
{$indent}{
{$indent}{$indent}return [
{$methodBody}
{$indent}{$indent}];
{$indent}}
PHP;
  }

  /**
   * @param Field[] $fields
   * @return string
   */
  private function buildToDeepArrayMethod(array $fields): string
  {
    $indent = $this->indentation;
    $doubledIndentation = str_repeat($indent, 2);

    $toDeepArrayCallsSourceCode = $this->buildToDeepArrayCalls($fields);

    if ($toDeepArrayCallsSourceCode === '') {
      $methodBody = "{$doubledIndentation}return \$this->toArray();";
    } else {
      $methodBody = <<<PHP
{$doubledIndentation}\$asArray = \$this->toArray();
{$toDeepArrayCallsSourceCode}
{$doubledIndentation}return \$asArray;
PHP;
    }

      return <<<PHP
public function toDeepArray(): array
{$indent}{
{$methodBody}
{$indent}}
PHP;
  }

  /**
   * @param Field[] $fields
   * @return string
   */
  private function buildFromArrayMethod(array $fields): string
  {
    $methodBody =  '';
    $indent = $this->indentation;
    $doubleIndentation = str_repeat($indent, 2);

    foreach ($fields as $field) {
      $setMethodName = $this->createSetterMethodName($field->name);
      $code = sprintf(
        'isset($properties[self::%1$s]) && $this->%2$s($properties[self::%1$s]);',
        $field->keyName,
        $setMethodName
      );
      $methodBody .= $doubleIndentation . $code . "\n";
    }

    $methodBody = rtrim($methodBody);

    return <<<PHP
public function fromArray(array \$properties): void
{$indent}{
{$methodBody}
{$indent}}
PHP;
  }

  private function isPrimitiveType(string $typeName): bool
  {
    $type = ltrim($typeName, '?');

    $map = [
      'bool' => true,
      'int' => true,
      'float' => true,
      'string' => true,
      'array' => true,
      'resource' => true,
    ];

    return $map[$type] ?? false;
  }

  /**
   * @param Field[] $fields
   * @return string
   */
  private function buildToDeepArrayCalls(array $fields): string
  {
    $doubledIndentation = str_repeat($this->indentation, 2);
    $toDeepArrayCalls = '';

    foreach ($fields as $field) {
      $code = '';

      if ($this->isChildOfBaseClass($field->rawType)) {
        $code = $this->buildToDeepArrayCodeLine($field);
      }

      if ($code !== '') {
        $toDeepArrayCalls .= "{$doubledIndentation}" . $code . "\n";
      }
    }

    return $toDeepArrayCalls;
  }

  private function buildToDeepArrayCodeLine(Field $field): string
  {
    $getMethodName = $this->createGetterMethodName($field->name);

    if ($field->type === 'array') {
      return sprintf('$asArray[self::%s] = $this->collectionToArray($this->%s());', $field->keyName, $getMethodName);
    }

    return sprintf('$asArray[self::%s] = $this->%s()->toDeepArray();', $field->keyName, $getMethodName);
  }

  private function isChildOfBaseClass(string $fieldType): bool
  {
    if ($this->isPrimitiveType($fieldType)) {
      return false;
    }

    return is_subclass_of($fieldType, 'Vijoni\ClassGenerator\ClassBase');
  }

  private function buildGetterMethodDocblock(Field $field): string
  {
    $indent = $this->indentation;
    if (!$this->isTypeOfCollection($field)) {
      return '';
    }

    return <<<DOCBLOCK
{$indent}/**
{$indent} * @return {$field->rawType}[]
{$indent} */

DOCBLOCK;
  }

  private function isTypeOfCollection(Field $field): bool
  {
    if ($field->type !== 'array') {
      return false;
    }

    if ($field->rawType === 'array') {
      return false;
    }

    return true;
  }
}
