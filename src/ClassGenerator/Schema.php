<?php

declare(strict_types=1);

namespace Vijoni\ClassGenerator;

class Schema
{
  public string $sourceFilePath = '';
  public string $className = '';
  public string $namespace = '';
  public string $outputPath = '';
  public array $fields = [];
  public array $constants = [];
}
