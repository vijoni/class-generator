<?php

declare(strict_types=1);

namespace Vijoni\ClassGenerator;

abstract class ClassBase
{
  protected array $modified = [];

  abstract public function toArray(): array;

  abstract public function fromArray(array $properties): void;

  abstract public function fieldKeys(): array;

  abstract public function toDeepArray(): array;

  public function readModifiedValues(): array
  {
    return array_intersect_key($this->toArray(), $this->modified);
  }

  public function resetModifiedValues(): void
  {
    $this->modified = [];
  }

  public function mapTo(array $mapKeys): array
  {
    $asArray = $this->toArray();
    $result = [];
    foreach ($mapKeys as $fromKey => $toKey) {
      $result[$toKey] = $asArray[$fromKey];
    }

    return $result;
  }

  public function mapModifiedTo(array $mapKeys): array
  {
    $asArray = $this->readModifiedValues();
    $result = [];
    foreach ($mapKeys as $fromKey => $toKey) {
      if (isset($asArray[$fromKey])) {
        $result[$toKey] = $asArray[$fromKey];
      }
    }

    return $result;
  }

  /**
   * @param self[] $collection
   * @return array
   */
  public function collectionToArray(array $collection): array
  {
    $asArray = [];
    foreach ($collection as $item) {
      $asArray[] = $item->toArray();
    }

    return $asArray;
  }

  public function intersectArray(array $map, array $source): array
  {
    $fieldKeys = $this->fieldKeys();
    $result = [];
    foreach ($map as $fromKey => $toKey) {
      if (array_key_exists($fromKey, $source) && array_key_exists($toKey, $fieldKeys)) {
        $result[$toKey] = $source[$fromKey];
      }
    }

    return $result;
  }

  public function intersect(array $map, array $source): void
  {
    $newValues = $this->intersectArray($map, $source);
    $this->fromArray($newValues);
  }
}
