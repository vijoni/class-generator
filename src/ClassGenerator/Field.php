<?php

declare(strict_types=1);

namespace Vijoni\ClassGenerator;

class Field
{
  public string $name = '';
  public string $type = '';
  public string $rawType = '';
  public string $defaultValue = '';
  public string $keyName = '';
}
