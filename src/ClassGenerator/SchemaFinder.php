<?php

declare(strict_types=1);

namespace Vijoni\ClassGenerator;

use Generator as Generator;

class SchemaFinder
{
  /**
   * @param string $dirPath
   * @param string $globPattern
   * @return Generator|Schema[]
   */
  public function findSchemas(string $dirPath, string $globPattern): Generator
  {
    $filePaths = $this->findFilePaths($dirPath, $globPattern);
    foreach ($filePaths as $schemaFilePath) {
      yield $this->readSchema($schemaFilePath);
    }
  }

  private function readSchema(string $schemaFilePath): Schema
  {
    /** @var array $schemaProperties */
    $schemaProperties = yaml_parse_file($schemaFilePath) ?: [];

    $schema = new Schema();
    $schema->sourceFilePath = $schemaFilePath;
    $schema->className = $schemaProperties['className'] ?? '';
    $schema->namespace = $schemaProperties['namespace'] ?? '';
    $schema->outputPath = $schemaProperties['outputPath'] ?? [];
    $schema->fields = $this->readFields($schemaProperties['fields'] ?? []);
    $schema->constants = $this->readConstants($schemaProperties['constants'] ?? []);

    return $schema;
  }

  private function findFilePaths(string $dirPath, string $globPattern): array
  {
    return $this->recursiveGlob($dirPath, $globPattern);
  }

  private function recursiveGlob(string $baseDir, string $pattern): array
  {
    $dir = rtrim($baseDir, '/');

    $filePaths = glob($dir . '/' . $pattern) ?: [];

    $directories = glob($dir . '/*', GLOB_ONLYDIR) ?: [];
    foreach ($directories as $directory) {
      $filePaths = array_merge($filePaths, $this->recursiveGlob($directory, $pattern));
    }

    return $filePaths;
  }

  private function readFields(array $schemaFields): array
  {
    $fields = [];

    foreach ($schemaFields as $fieldName => $fieldSpec) {
      $tokens = explode(',', $fieldSpec);
      $definedType = trim($tokens[0]);
      $fieldType = $this->readType($definedType);
      $defaultValue = $tokens[1] ?? $this->readDefaultValueByType($fieldType);

      $field = new Field();
      $field->name = $fieldName;
      $field->type = $fieldType;
      $field->rawType = $this->extractFieldType($definedType);
      $field->defaultValue = $defaultValue;
      $field->keyName = $this->toUpperCase($fieldName);

      $fields[$fieldName] = $field;
    }

    return $fields;
  }

  private function readDefaultValueByType(string $type): string
  {
    if (str_starts_with($type, '?')) {
      return 'null';
    }

    $map = [
      'string' => "''",
      'int' => '-1',
      'float' => '-1.0',
      'array' => '[]',
      'bool' => 'false',
    ];

    return $map[$type] ?? '';
  }

  private function toUpperCase(string $input): string
  {
    $output = (string)preg_replace('/([A-Z]+)/', '_$1', $input);

    return strtoupper($output);
  }

  private function readConstants(array $schemaConstants): array
  {
    $constants = [];

    foreach ($schemaConstants as $constantName => $constantValue) {
      $constant = new Constant();
      $constant->name = $constantName;
      $constant->value = $constantValue;

      $constants[$constantName] = $constant;
    }

    return $constants;
  }

  private function readType(string $type): string
  {
    if (str_ends_with($type, '[]')) {
      return 'array';
    }

    return $type;
  }

  private function extractFieldType(string $type): string
  {
    $parsedType = rtrim($type, '[]');

    return ltrim($parsedType, '?');
  }
}
