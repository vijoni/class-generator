<?php

declare(strict_types=1);

namespace Vijoni\ClassGenerator;

class Constant
{
  public string $name = '';
  public string $value = '';
}
