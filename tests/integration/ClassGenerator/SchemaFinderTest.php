<?php

declare(strict_types=1);

namespace VijoniTest\Integration\Generator;

use Codeception\Test\Unit;
use Vijoni\ClassGenerator\Constant;
use Vijoni\ClassGenerator\Field;
use Vijoni\ClassGenerator\Schema;
use Vijoni\ClassGenerator\SchemaFinder;

class SchemaFinderTest extends Unit
{
  public function testFindSchemaFiles(): void
  {
    $schemaFinder = new SchemaFinder();
    $schemas = $schemaFinder->findSchemas(__DIR__ . '/fixtures', 'class-schema-*.yml');

    $expected = [
      ['VijoniOutput\Integration\Security\Shared\Generated', 'GeneratedRole'],
      ['VijoniOutput\Integration\Security\Shared\Generated', 'GeneratedSecret'],
      ['VijoniOutput\Integration\Security\Shared\Generated', 'GeneratedUser'],
      ['VijoniOutput\Integration\Shop\Shared\Generated', 'GeneratedCustomer'],
    ];

    $i = 0;
    foreach ($schemas as $schema) {
      $this->assertSame($expected[$i][0], $schema->namespace);
      $this->assertSame($expected[$i][1], $schema->className);
      ++$i;
    }
  }

  public function testPrimitiveSchemaData(): void
  {
    $schemaFinder = new SchemaFinder();
    $schemas = $schemaFinder->findSchemas(__DIR__ . '/fixtures', 'class-schema-customer.yml');

    /** @var Schema $schema */
    $schema = $schemas->current();
    $this->assertStringEndsWith('Shop/Shared/class-schema-customer.yml', $schema->sourceFilePath);
    $this->assertSame('_output/Integration/Shop/Shared/Generated', $schema->outputPath);
    $this->assertSame('VijoniOutput\Integration\Shop\Shared\Generated', $schema->namespace);
    $this->assertSame('GeneratedCustomer', $schema->className);

    /** @var Field $idField */
    $idField = $schema->fields['id'];
    $this->assertSame('id', $idField->name);
    $this->assertSame('string', $idField->type);
    $this->assertSame('string', $idField->rawType);
    $this->assertSame("''", $idField->defaultValue);
    $this->assertSame('ID', $idField->keyName);

    /** @var Constant $defaultDateConstant */
    $defaultDateConstant = $schema->constants['DEFAULT_DATE'];
    $this->assertSame('DEFAULT_DATE', $defaultDateConstant->name);
    $this->assertSame('1900-01-01 00:00:00', $defaultDateConstant->value);
  }

  public function testCollectionSchemaData(): void
  {
    $schemaFinder = new SchemaFinder();
    $schemas = $schemaFinder->findSchemas(__DIR__ . '/fixtures', 'class-schema-user.yml');

    /** @var Schema $schema */
    $schema = $schemas->current();

    /** @var Field $idField */
    $idField = $schema->fields['roles'];
    $this->assertSame('roles', $idField->name);
    $this->assertSame('array', $idField->type);
    $this->assertSame('\VijoniTest\Integration\Security\Shared\Generated\GeneratedRole', $idField->rawType);
    $this->assertSame("[]", $idField->defaultValue);
    $this->assertSame('ROLES', $idField->keyName);
  }

  public function testNullSchemaData(): void
  {
    $schemaFinder = new SchemaFinder();
    $schemas = $schemaFinder->findSchemas(__DIR__ . '/fixtures', 'class-schema-user.yml');

    /** @var Schema $schema */
    $schema = $schemas->current();

    /** @var Field $idField */
    $idField = $schema->fields['comment'];
    $this->assertSame('comment', $idField->name);
    $this->assertSame('?string', $idField->type);
    $this->assertSame('string', $idField->rawType);
    $this->assertSame("null", $idField->defaultValue);
    $this->assertSame('COMMENT', $idField->keyName);
  }
}
