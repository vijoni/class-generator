<?php

declare(strict_types=1);

namespace VijoniTest\Integration\ClassGenerator;

use DateTime;
use VijoniOutput\Integration\Shop\Shared\Generated\GeneratedCustomer;

class Customer extends GeneratedCustomer
{
  public function __construct()
  {
    $this->setBirthDate(new DateTime(static::DEFAULT_DATE));
    $this->resetModifiedValues();
  }
}
