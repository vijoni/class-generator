<?php

declare(strict_types=1);

namespace VijoniTest\Integration\Generator;

use Codeception\Test\Unit;
use Vijoni\ClassGenerator\Generator;
use Vijoni\ClassGenerator\Schema;
use Vijoni\ClassGenerator\SchemaFinder;
use VijoniOutput\Integration\Security\Shared\Generated\GeneratedRole;
use VijoniOutput\Integration\Security\Shared\Generated\GeneratedUser;
use VijoniTest\Helper\ClassGenerator\ConstantBuilder;
use VijoniTest\Helper\ClassGenerator\FieldBuilder;
use VijoniTest\Integration\ClassGenerator\Customer;
use VijoniTest\IntegrationTester;

class GeneratorTest extends Unit
{
  protected IntegrationTester $tester;

  private const CLASS_FILE_PATHS = [
    'Integration/Shop/Shared/Generated/GeneratedCustomer.php',
    'Integration/Security/Shared/Generated/GeneratedSecret.php',
    'Integration/Security/Shared/Generated/GeneratedUser.php',
    'Integration/Security/Shared/Generated/GeneratedRole.php',
  ];

  private static bool $isFirstTime = true;

  protected function _before(): void
  {
    //_setUpBeforeClass is called for every suite causing this code to be executed not only for this class
    if (self::$isFirstTime) {
      foreach (self::CLASS_FILE_PATHS as $filePath) {
        @unlink(codecept_output_dir($filePath));
      }

      $schemas = [
        $this->buildCustomerSchema(),
        $this->buildSecretSchema(),
        $this->buildUserSchema(),
        $this->buildRoleSchema(),
      ];

      $this->generateClasses($schemas);
      self::$isFirstTime = false;
    }
  }

  public function testAccessorMethods(): void
  {
    $customer = new Customer();
    $faker = $this->tester->faker();

    $firstname = $faker->firstName();
    $customer->setFirstname($firstname);
    $this->assertSame($firstname, $customer->getFirstname());

    $birthDate = $faker->dateTime();
    $customer->setBirthDate($birthDate);
    $this->assertSame($birthDate, $customer->getBirthDate());

    $score = $faker->randomNumber(1, true);
    $customer->setScore($score);
    $this->assertSame($score, $customer->getScore());
  }

  public function testGetModifiedProperties(): void
  {
    $customer = new Customer();
    $faker = $this->tester->faker();

    $firstname = $faker->firstName();
    $customer->setFirstname($firstname);

    $this->assertSame(
      [
        Customer::FIRSTNAME => $firstname,
      ],
      $customer->readModifiedValues()
    );
  }

  public function testToArray(): void
  {
    $customer = new Customer();
    $faker = $this->tester->faker();

    $firstname = $faker->firstName();
    $customer->setFirstname($firstname);

    $score = $faker->randomNumber(1, true);
    $customer->setScore($score);

    $birthDate = $faker->dateTime();
    $customer->setBirthDate($birthDate);

    $this->assertSame(
      [
        Customer::FIRSTNAME => $firstname,
        Customer::BIRTH_DATE => $birthDate,
        Customer::SCORE => $score,
      ],
      $customer->toArray()
    );
  }

  public function testToDeepArray(): void
  {
    $user = new GeneratedUser();
    $asArray = $user->toDeepArray();
    $this->assertSame(
      [
        'id' => '',
        'email' => '',
        'secret' => [
          'secret' => '',
          'seed' => -1,
        ],
        'secretClone' => [
          'secret' => '',
          'seed' => -1,
        ],
        'roles' => [],
        'comment' => null,
      ],
      $asArray
    );
  }

  public function testFromArray(): void
  {
    $faker = $this->tester->faker();
    $firstname = $faker->firstName();
    $birthDate = $faker->dateTime();

    $properties = [
      Customer::FIRSTNAME => $firstname,
      Customer::BIRTH_DATE => $birthDate,
    ];

    $customer = new Customer();
    $customer->fromArray($properties);

    $this->assertSame($firstname, $customer->getFirstname());
    $this->assertSame(-1, $customer->getScore());
    $this->assertSame($birthDate, $customer->getBirthDate());
  }

  public function testCollectionToArray(): void
  {
    $roleAdmin = new GeneratedRole();
    $roleAdmin->setName('admin');
    $roleManager = new GeneratedRole();
    $roleManager->setName('manager');

    $user = new GeneratedUser();
    $user->setRoles([
      $roleAdmin,
      $roleManager,
    ]);

    $userRoles = $user->collectionToArray($user->getRoles());

    $this->assertSame(
      [
        ['name' => 'admin'],
        ['name' => 'manager'],
      ],
      $userRoles
    );
  }

  public function testIntersectArray(): void
  {
    $faker = $this->tester->faker();
    $id = $faker->uuid();
    $email = $faker->email();

    $request = [
      'id' => $id,
      'email' => $email,
    ];

    $user = new GeneratedUser();
    $intersection = $user->intersectArray(
      [
        'id' => GeneratedUser::ID,
        'email' => GeneratedUser::EMAIL,
      ],
      $request
    );

    $this->assertSame(
      [
        GeneratedUser::ID => $id,
        GeneratedUser::EMAIL => $email,
      ],
      $intersection
    );
  }

  public function testIntersect(): void
  {
    $faker = $this->tester->faker();
    $id = $faker->uuid();
    $email = $faker->email();

    $request = [
      'id' => $id,
      'email' => $email,
    ];

    $user = new GeneratedUser();
    $user->intersect(
      [
        'id' => GeneratedUser::ID,
        'email' => GeneratedUser::EMAIL,
      ],
      $request
    );

    $this->assertSame($id, $user->getId());
    $this->assertSame($email, $user->getEmail());
  }

  public function testMapTo(): void
  {
    $customer = new Customer();
    $faker = $this->tester->faker();

    $firstname = $faker->firstName();
    $customer->setFirstname($firstname);

    $birthDate = $faker->dateTime();
    $customer->setBirthDate($birthDate);

    $mappedValues = $customer->mapTo([
      Customer::FIRSTNAME => 'name',
      Customer::BIRTH_DATE => 'dob',
    ]);

    $this->assertSame(
      [
        'name' => $firstname,
        'dob' => $birthDate,
      ],
      $mappedValues
    );
  }

  public function testMapModifiedTo(): void
  {
    $customer = new Customer();
    $faker = $this->tester->faker();

    $firstname = $faker->firstName();
    $customer->setFirstname($firstname);

    $mappedValues = $customer->mapModifiedTo([
      Customer::FIRSTNAME => 'name',
      Customer::BIRTH_DATE => 'dob',
    ]);

    $this->assertSame(
      [
        'name' => $firstname,
      ],
      $mappedValues
    );
  }

  public function testMissingDependency(): void
  {
    $schemas = [
      $this->buildCustomerSchema(),
      $this->buildSchemaWithMissingDependency(),
      $this->buildSecretSchema(),
    ];
    $missingSchemas = $this->generateClasses($schemas);
    $this->assertCount(1, $missingSchemas);
    $this->assertSame('GeneratedStore', $missingSchemas[0]->className);
  }

  /**
   * @param Schema[] $schemas
   * @return Schema[]
   */
  private function generateClasses(array $schemas): array
  {
    $srcDirectory = codecept_output_dir() . '../';
    $classGenerator = new Generator($srcDirectory, $this->mockSchemaFinder($schemas));

    return $classGenerator->generate('any');
  }

  private function mockSchemaFinder(array $schemas): SchemaFinder
  {
    $filePathsGenerator = (static function () use ($schemas) {
      foreach ($schemas as $schema) {
        yield $schema;
      }
    })();

    $schemaFinder = $this->createMock(SchemaFinder::class);
    $schemaFinder->method('findSchemas')->willReturn($filePathsGenerator);

    return $schemaFinder;
  }

  private function buildCustomerSchema(): Schema
  {
    $schema = new Schema();
    $schema->className = 'GeneratedCustomer';
    $schema->namespace = 'VijoniOutput\Integration\Shop\Shared\Generated';
    $schema->outputPath = '_output/Integration/Shop/Shared/Generated';
    $schema->fields = [
      'firstname' => FieldBuilder::buildField('firstname', 'string', 'string', "''", 'FIRSTNAME'),
      'birthDate' => FieldBuilder::buildField('birthDate', '\DateTime', '\DateTime', '', 'BIRTH_DATE'),
      'score' => FieldBuilder::buildField('score', 'int', 'int', '-1', 'SCORE'),
    ];

    $schema->constants = [
      'DEFAULT_DATE' => ConstantBuilder::buildConstant('DEFAULT_DATE', "'1900-01-01 00:00:00'"),
    ];

    return $schema;
  }

  private function buildUserSchema(): Schema
  {
    $schema = new Schema();
    $schema->className = 'GeneratedUser';
    $schema->namespace = 'VijoniOutput\Integration\Security\Shared\Generated';
    $schema->outputPath = '_output/Integration/Security/Shared/Generated';
    $schema->fields = [
      'id' => FieldBuilder::buildField('id', 'string', 'string', "''", 'ID'),
      'email' => FieldBuilder::buildField('email', 'string', 'string', "''", 'EMAIL'),
      'secret' => FieldBuilder::buildField(
        'secret',
        '\VijoniOutput\Integration\Security\Shared\Generated\GeneratedSecret',
        '\VijoniOutput\Integration\Security\Shared\Generated\GeneratedSecret',
        '',
        'SECRET'
      ),
      // used to check constructor generated code indentation
      'secretClone' => FieldBuilder::buildField(
        'secretClone',
        '\VijoniOutput\Integration\Security\Shared\Generated\GeneratedSecret',
        '\VijoniOutput\Integration\Security\Shared\Generated\GeneratedSecret',
        '',
        'SECRET_CLONE'
      ),
      'roles' => FieldBuilder::buildField(
        'roles',
        'array',
        '\VijoniOutput\Integration\Security\Shared\Generated\GeneratedRole',
        '[]',
        'ROLES',
      ),
      'comment' => FieldBuilder::buildField(
        'comment',
        '?string',
        'string',
        'null',
        'COMMENT',
      ),
    ];

    return $schema;
  }

  private function buildSecretSchema(): Schema
  {
    $schema = new Schema();
    $schema->className = 'GeneratedSecret';
    $schema->namespace = 'VijoniOutput\Integration\Security\Shared\Generated';
    $schema->outputPath = '_output/Integration/Security/Shared/Generated';
    $schema->fields = [
      'secret' => FieldBuilder::buildField('secret', 'string', 'string', "''", 'SECRET'),
      'seed' => FieldBuilder::buildField('seed', 'int', 'int', '-1', 'SEED'),
    ];

    return $schema;
  }

  private function buildRoleSchema(): Schema
  {
    $schema = new Schema();
    $schema->className = 'GeneratedRole';
    $schema->namespace = 'VijoniOutput\Integration\Security\Shared\Generated';
    $schema->outputPath = '_output/Integration/Security/Shared/Generated';
    $schema->fields = [
      'name' => FieldBuilder::buildField('name', 'string', 'string', "''", 'NAME'),
    ];

    return $schema;
  }

  private function buildSchemaWithMissingDependency(): Schema
  {
    $schema = new Schema();
    $schema->className = 'GeneratedStore';
    $schema->namespace = 'VijoniOutput\Integration\Security\Shared\Generated';
    $schema->outputPath = '_output/Integration/Missing/Shared/Generated';
    $schema->fields = [
      'id' => FieldBuilder::buildField('id', 'string', 'string', "''", 'ID'),
      'missing' => FieldBuilder::buildField(
        'missing',
        '\VijoniOutput\Integration\Security\Shared\Generated\GeneratedMissing',
        '\VijoniOutput\Integration\Security\Shared\Generated\GeneratedMissing',
        '',
        'MISSING'
      ),
    ];

    return $schema;
  }
}
