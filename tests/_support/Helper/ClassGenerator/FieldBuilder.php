<?php

declare(strict_types=1);

namespace VijoniTest\Helper\ClassGenerator;

use Vijoni\ClassGenerator\Field;

class FieldBuilder
{
  public static function buildField(
    string $name,
    string $type,
    string $rawType,
    string $defaultValue,
    string $keyName,
  ): Field {
    $field = new Field();
    $field->name = $name;
    $field->type = $type;
    $field->rawType = $rawType;
    $field->defaultValue = $defaultValue;
    $field->keyName = $keyName;

    return $field;
  }
}
