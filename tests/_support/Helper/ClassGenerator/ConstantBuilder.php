<?php

declare(strict_types=1);

namespace VijoniTest\Helper\ClassGenerator;

use Vijoni\ClassGenerator\Constant;

class ConstantBuilder
{
  public static function buildConstant(
    string $name,
    string $value,
  ): Constant {
    $constant = new Constant();
    $constant->name = $name;
    $constant->value = $value;

    return $constant;
  }
}
