<?php

declare(strict_types=1);

namespace VijoniTest\Acceptance\Generator;

use Codeception\Test\Unit;
use Vijoni\ClassGenerator\Generator;
use Vijoni\ClassGenerator\SchemaFinder;
use VijoniOutput\Acceptance\Shop\Shared\Generated\GeneratedCustomer;

class GeneratorTest extends Unit
{
  public function testGeneratedClass(): void
  {
    $srcDirectory = codecept_output_dir() . '../';

    $classGenerator = new Generator($srcDirectory, new SchemaFinder());
    $classGenerator->generate('acceptance/ClassGenerator/fixtures/Shop/Shared/class-schema-customer.yml');

    $generatedCustomer = new GeneratedCustomer();
    $this->assertInstanceOf(GeneratedCustomer::class, $generatedCustomer);
  }
}
